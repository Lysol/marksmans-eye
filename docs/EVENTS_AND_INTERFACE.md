## Events & Interface

This page describes how to hook into Marksman's Eye from other mods.

#### Events

There are no events at this time.

#### Interface

Use it like this:

```lua
local I = require("openmw.interfaces")
local ui = require("openmw.ui")

local function marksmansEyeLevel()
  if I.MarksmansEye then
    local msg = "Marksman's Eye level: %s"
    ui.ShowMessage(msg, I.MarksmansEye.Level())
  end
end
```

##### `Level()`

Returns a string that indicates whether or not the Marksman's Eye ability is active on the player (meaning they possess the required item, "Not Active" otherwise), and if it is what the level of that ability ("Low", "Medium", "High", and "Maximum") is for the player.
