## Marksman's Eye Changelog

#### Version 1.2

* Correctly implemented the PL and PT_BR localizations
* Added DE, SV localizations (thanks Atahualpa, Lysol!)
* Added contact details to the README

<!-- [Download Link](https://gitlab.com/modding-openmw/marksmans-eye/-/packages/TODO) | [Nexus](https://www.nexusmods.com/morrowind/mods/51141) -->

#### Version 1.1

* Improved some lines of dialogue.
* Made the initial "little secret" a little less common.
* Included a hint about where to find a certain NPC in a dialogue/jounral entry.
* Added a mod settings menu.
* Made the zoom effect intensity configurable via the settings menu (up to a certain limit). Note that your player character still needs to have the requisite skill to hit whatever limit you've set.

[Download Link](https://gitlab.com/modding-openmw/marksmans-eye/-/packages/7081149) | [Nexus](https://www.nexusmods.com/morrowind/mods/51141)

#### Version 1.0

Initial mod release.

Features:

* A hidden magical artifact that enables a special "zoom" effect when aiming a bow or crossbow.
* This effect scales with the player's Marksman skill and Speed attribute. Marksman weighs more in this equation.
* The effect is only active when the player has the artifact in their inventory.
* A small quest to give the player a hint about the artifact's existence and where to find it.
* The `Level()` interface to provide other mods with a reading of how intense the zoom effect will be based on the player's stats.

[Download Link](https://gitlab.com/modding-openmw/marksmans-eye/-/packages/6602719) | [Nexus](https://www.nexusmods.com/morrowind/mods/51141)
